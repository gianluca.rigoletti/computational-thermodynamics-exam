# File to plot the results
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from pathlib import Path


def read_single_simulation(filepath):
    loaded = np.load(filepath)
    ekin = loaded['ekin']
    epot = loaded['epot']
    tinstant = loaded['tinstant']
    # parse the configuration parameters from the filename
    filepath = Path(filepath)
    name = filepath.stem
    pieces = name.split('_')
    simulation_number = int(pieces[0][-1])  # it is the last character of simulation1, simulation2, etc.)
    temperature = int(pieces[2])
    timestep = float(pieces[4])
    return ekin, epot, tinstant, simulation_number, temperature, timestep


def compute_useful_statistics(ekin, epot, tinstant):
    etot = ekin + epot
    # calculate the relative fluctuation of energy deltaE / E
    fluctuation = etot.std() / etot.mean()
    # Calculate the average temperature
    tmean = tinstant.mean()
    return fluctuation, tmean


def results_to_df(resultspath):
    resultspath = Path(resultspath)
    results = []
    for file in resultspath.glob('*.npz'):
        ekin, epot, tinstant, simulation_number, temperature, timestep = read_single_simulation(file)
        fluctuation, tmean = compute_useful_statistics(ekin, epot, tinstant)
        results.append(dict(
            simulation_number=simulation_number,
            temperature=temperature,
            timestep=timestep,
            fluctuation=fluctuation,
            tmean=tmean
        ))

    df = pd.DataFrame(results)
    return df


def main():
    resultspath = '../results'
    df = results_to_df(resultspath).sort_values(by='simulation_number')
    print(df)


if __name__ == '__main__':
    main()
