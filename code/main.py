from numba import jit
import numpy as np
from functions import *
import json
from pathlib import Path
import matplotlib.pyplot as plt
from matplotlib.animation import FFMpegWriter
from mpl_toolkits.mplot3d import Axes3D



# @jit(nopython=True, cache=True)
def start_simulation(positions, sx, sy, sz, natoms, seed, mass, temperature, rprime, rcutoff,
                     epsilon, sigma, thermalizationtime, simulationtime, timestep, ranv,
                     ekin, teff, tshifted, tnew, nn, fx, fy, fz, epot):
    niterations = int(simulationtime / timestep)
    niterations_eq = int(thermalizationtime / timestep)
    niteration_interesting = niterations - niterations_eq
    print('Number of iterations to be done: ', niterations)
    print('Number of iterations to reach thermalization: ', niterations_eq)
    print('Number of actually interesting iterations: ', niteration_interesting)
    printProgressBar(0, niterations, prefix='Progress:', suffix='Complete', length=50)

    # Create an array to store the interesting data about the simulation
    # The data will be the one after the thermalization time
    ekin = np.full(niterations, np.nan)
    epotarr = np.full(niterations, np.nan)
    tinstant = np.full(niterations, np.nan)
    vx, vy, vz = ranv[:, 0], ranv[:, 1], ranv[:, 2]
    # Positions is a 3xnatoms matrix. To unpack it to three variables you need
    # to transpose the matrix
    x, y, z = positions[:, 0], positions[:, 1], positions[:, 2]

    metadata = dict(title='Movie Test', artist='Matplotlib',
                comment='Movie support!')
    writer = FFMpegWriter(fps=15, metadata=metadata)

    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    title = ax.text(0.5, 1.05, 1.05, '', transform=ax.transAxes, verticalalignment='center')
    graph = ax.scatter(x, y, z, s=300)

    with writer.saving(fig, f"writer_test_{niterations}.mp4", 100):
        for i in range(niterations):
            if (i % 50 == 0 or i == niterations - 1):
                printProgressBar(i + 1, niterations, prefix='Progress:', suffix='Complete', length=50)
                pstime = i * timestep / 1e-12
                text = f'Temperature {temperature}K, time {pstime:.3f} ps'
                title.set_text(text)
            # Compute the new positions with the velocity verlet algorithm
            for c in range(natoms):
                x[c] = x[c] + vx[c] * timestep + 1 / 2 * fx[c] / mass * timestep**2
                y[c] = y[c] + vy[c] * timestep + 1 / 2 * fy[c] / mass * timestep**2
                z[c] = z[c] + vz[c] * timestep + 1 / 2 * fz[c] / mass * timestep**2
                # TODO: add option to enable verlet cages to speed up the simulation
            positions = np.array([x, y, z]).T
            # Recompute neighbors
            nn = neighbors(positions, rcutoff=rcutoff)
            oldfx, oldfy, oldfz = fx, fy, fz

            # Recompute the forces and the potential energy
            fx, fy, fz, epot = calclj(positions, nn, rprime, rcutoff, sx, sy, sz,
                                    epsilon=epsilon, sigma=sigma)

            # Recompute the new velocities
            for c in range(natoms):
                vx[c] = vx[c] + 1 / (2 * mass) * (oldfx[c] + fx[c]) * timestep
                vy[c] = vy[c] + 1 / (2 * mass) * (oldfy[c] + fy[c]) * timestep
                vz[c] = vz[c] + 1 / (2 * mass) * (oldfz[c] + fz[c]) * timestep

            # If the equilibration time has passed start saving data
            # if i >= niterations_eq:
            #     ix = i - niterations_eq
            results = calcekin(vx, vy, vz, mass)
            ekin[i], tinstant[i] = results[0], results[1]
            epotarr[i] = epot
            # Write to file as a movie
            graph._offsets3d = (x, y, z)
            writer.grab_frame()


    return ekin, epotarr, tinstant


def main():
    # configs.json contains a set of parameters that define
    # different simulations. Every parameters inside configs.json
    # can be changed (for example the timestep or the temperature)

    # Note for me: to debug code I need to comment out the @jit
    # decorator from the functions otherwise numba will compile the function
    # in the cache and vscode can't use the step by step debugger
    with open('configs.json') as f:
        configs = json.load(f)
    # Read the positions of the atoms in the lattice together with
    # the lattice spacing
    print(json.dumps(configs['simulations'], indent=2))
    for simulation in configs['simulations']:
        name = simulation['name']  # something like simulation1, simulation2, etc.
        positions, sx, sy, sz = readfile(simulation['filepath'])
        natoms = positions.shape[0]
        seed = simulation['seed']
        mass = simulation['mass']
        temperature = simulation['temperature']
        rprime = simulation['rprime']
        rcutoff = simulation['rcutoff']
        thermalizationtime = simulation['thermalization_time']
        simulationtime = simulation['simulation_time']
        timestep = simulation['timestep']
        epsilon = simulation['epsilon']
        sigma = simulation['sigma']
        print('Initializing velocities...')
        ranv, ekin, teff, tshifted, tnew = initvelocities(natoms, temperature, mass, seed)
        print('...done')
        print('Computing the neighbors matrix...')
        nn = neighbors(positions, rcutoff=rcutoff)
        print('...done')
        print('Calculating the first forces and potential energy...')
        fx, fy, fz, epot = calclj(positions, nn, rprime, rcutoff, sx, sy, sz, epsilon=epsilon, sigma=sigma)
        print('...done')
        print(f'Starting {name}')
        ekin, epot, tinstant = start_simulation(positions, sx, sy, sz, natoms, seed, mass, temperature, rprime, rcutoff,
                                                epsilon, sigma, thermalizationtime, simulationtime, timestep,
                                                ranv, ekin, teff, tshifted, tnew, nn, fx, fy, fz, epot)

        Path('../results').mkdir(exist_ok=True)
        np.savez_compressed(f'../results/{name}_t_{temperature}_ts_{timestep:.1e}.npz',
                            ekin=ekin, epot=epot, tinstant=tinstant)


if __name__ == '__main__':
    main()
