import numpy as np
from numba import jit


def readfile(filepath):
    """Function for reading file. The first line is the
    lattice spacing
    """
    data = np.genfromtxt(filepath)
    positions = data[1:, :]
    [sx, sy, sz] = data[0, :]
    return positions, sx, sy, sz

# Print iterations progress
# from https://stackoverflow.com/questions/3173320/text-progress-bar-in-the-console


def printProgressBar(iteration, total, prefix='', suffix='', decimals=1, length=100, fill='█'):
    """
    Call in a loop to create terminal progress bar
    @params:
        iteration   - Required  : current iteration (Int)
        total       - Required  : total iterations (Int)
        prefix      - Optional  : prefix string (Str)
        suffix      - Optional  : suffix string (Str)
        decimals    - Optional  : positive number of decimals in percent complete (Int)
        length      - Optional  : character length of bar (Int)
        fill        - Optional  : bar fill character (Str)
    """
    percent = ("{0:." + str(decimals) + "f}").format(100 * (iteration / float(total)))
    filledLength = int(length * iteration // total)
    bar = fill * filledLength + '-' * (length - filledLength)
    print('\r%s |%s| %s%% %s' % (prefix, bar, percent, suffix), end='\r')
    # Print New Line on Complete
    if iteration == total:
        print()


@jit
def calcdist(point1, point2):
    """Calculate the distance between two points in a
    3D space"""
    return np.sqrt(np.sum(np.square(point1 - point2)))


@jit(nopython=True)
def neighbors(positions, rcutoff=4.625, pbc=False):
    """Given the positions of the atoms return a nxn matrix
    with the list of the neighbors within a distance of rcutoff.
    The matrix is initally filled with -1, so if the number of
    neighbors is less than n there will be -1 on all the non-filled
    slots.
    """

    natoms = positions.shape[0]
    nn = np.full((natoms, natoms), -1, dtype=np.int32)

    for i in range(natoms):
        ipos = positions[i, :]
        neighbornumber = 0
        for j in range(natoms):
            jpos = positions[j, :]
            if i != j:
                totdist = calcdist(ipos, jpos)
                # TODO: add an option to enable/disable PBC
                if pbc:
                    pass
                if totdist < rcutoff:
                    nn[i, neighbornumber] = j
                    neighbornumber += 1
    return nn


def initvelocities(natoms, temperature, mass, seed):
    # Seed the generator so we can get consisten simulations
    np.random.seed(seed)
    # Define the constant coming from the maxwellian distribution
    # and the equipartion theorem
    const = np.sqrt(3 * temperature / (11603 * mass))
    # Create a vector of random numbers with natoms rows and 3 columns
    # because of vx, vy and vz. Extrema are set in order to
    # have the same area as the maxwellina distribution
    ranv = np.random.uniform(-const, +const, size=(natoms, 3))
    # Calculate the quadratic velocities of all the entries
    ranv2 = np.square(ranv)
    # Compute the average values for each column
    meanv = ranv.mean(axis=0)
    meanv2 = ranv2.mean(axis=0)
    # Compute the average kinetic energy (it will be an array of
    # 3 elements)
    ekin = 1 / 2 * mass * meanv2
    # Compute the temperature from the equipartition theorem
    teff = 1 / 3 * mass * 11603 * np.sum(meanv2)
    # Now shift the velocities in order to have the crystal
    # with the center of mass still. If we don't do the following
    # steps we will see the center of mass of the crystal moving
    # and the average temperature won't be the one we want
    # To shift it we will subtract the average value from every single
    # velocity, calculate the new temperature and rescale
    ranv = ranv - meanv
    ranv2 = np.square(ranv)
    meanv = ranv.mean(axis=0)
    meanv2 = ranv2.mean(axis=0)
    teff_shifted = 1 / 3 * mass * 11603 * np.sum(meanv2)
    # Now rescale the velocitites for the factor sqrt(Teff/Teff_shifted)
    ranv = ranv * np.sqrt(temperature / teff_shifted)
    ranv2 = np.square(ranv)
    meanv = ranv.mean(axis=0)
    meanv2 = ranv2.mean(axis=0)
    teff_new = 1 / 3 * mass * 11603 * np.sum(meanv2)
    ekin = 1 / 2 * mass * meanv2
    print(f'Initial mean velocitiy: {meanv}')
    return ranv, ekin, teff, teff_shifted, teff_new


@jit(nopython=True)
def ljforce(totdist, dist, epsilon=0.345, sigma=2.644):
    return 24 * epsilon * sigma**6 * dist * (2 * sigma**6 / totdist**6 - 1) / totdist**8


@jit(nopython=True)
def ljpot(dist, epsilon=0.345, sigma=2.644):
    return 4 * epsilon * ((sigma / dist)**12 - (sigma / dist)**6)


@jit
def poldegrees(rp, rc, epsilon=0.345, sigma=2.644):
    Apoli7 = (1/((rc - rp)**7*rp**12))*4*epsilon*rc**4*sigma**6*(2 * rp**6 * (-42 * rc**3 + 182 * rc**2 * rp - 273 *
                                                                              rc * rp**2 + 143 * rp**3) + (455 * rc**3 - 1729 * rc**2 * rp + 2223 * rc * rp**2 - 969 * rp**3)*sigma**6)
    Bpoli7 = (1/((rc - rp)**7 * rp**13))*16 * epsilon * rc**3 * sigma**6*(rp**6 * (54 * rc**4 - 154 * rc**3 * rp + 351 * rc *
                                                                                   rp**3 - 286 * rp**4) + (-315 * rc**4 + 749 * rc**3 * rp + 171 * rc**2 * rp**2 - 1539 * rc * rp**3 + 969 * rp**4) * sigma**6)
    Cpoli7 = (1/((rc - rp)**7 * rp**14))*12 * epsilon * rc**2 * sigma**6 * (rp**6 * (-63 * rc**5 - 7 * rc**4 * rp + 665 * rc**3 * rp**2 - 975 * rc**2 * rp**3 -
                                                                                     52 * rc * rp**4 + 572 * rp**5) + 2 * (195 * rc**5 + 91 * rc**4 * rp - 1781 * rc**3 * rp**2 + 1995 * rc**2 * rp**3 + 399 * rc * rp**4 - 969 * rp**5) * sigma**6)
    Dpoli7 = (1/((rc - rp)**7 * rp**15))*16 * epsilon * sigma**6*(rc * rp**6 * (14 * rc**6 + 126 * rc**5 * rp - 420 * rc**4 * rp**2 - 90 * rc**3 * rp**3 + 1105 * rc**2 * rp**4 - 624 *
                                                                                rc * rp**5 - 286 * rp**6) + rc * (-91 * rc**6 - 819 * rc**5 * rp + 2145 * rc**4 * rp**2 + 1125 * rc**3 * rp**3 - 5035 * rc**2 * rp**4 + 1881 * rc * rp**5 + 969 * rp**6) * sigma**6)
    Epoli7 = (1/((rc - rp)**7 * rp**15))*4 * epsilon * sigma**6*(2 * rp**6 * (-112 * rc**6 - 63 * rc**5 * rp + 1305 * rc**4 * rp**2 - 1625 * rc**3 * rp**3 - 585 * rc**2 * rp**4 + 1287 *
                                                                              rc * rp**5 + 143 * rp**6) + (1456*rc**6 + 1404*rc**5 * rp - 14580 * rc**4 * rp**2 + 13015 * rc**3 * rp**3 + 7695 * rc**2 * rp**4 - 8721 * rc * rp**5 - 969 * rp**6) * sigma**6)
    Fpoli7 = (1/((rc - rp)**7 * rp**15))*48 * epsilon * sigma**6*(-rp**6 * (-28 * rc**5 + 63 * rc**4 * rp + 65 * rc**3 * rp**2 - 247 * rc**2 * rp**3 + 117 *
                                                                            rc * rp**4 + 65 * rp**5) + (-182 * rc**5 + 312 * rc**4 * rp + 475 * rc**3 * rp**2 - 1140 * rc**2 * rp**3 + 342 * rc * rp**4 + 228 * rp**5) * sigma**6)
    Gpoli7 = (1/((rc - rp)**7 * rp**15))*4 * epsilon * sigma**6 * (rp**6 * (-224 * rc**4 + 819 * rc**3 * rp - 741 * rc**2 * rp**2 - 429 *
                                                                            rc * rp**3 + 715 * rp**4) + 2 * (728 * rc**4 - 2223 * rc**3 * rp + 1425 * rc**2 * rp**2 + 1292 * rc * rp**3 - 1292 * rp**4) * sigma**6)
    Hpoli7 = (1/((rc - rp)**7 * rp**15))*16 * epsilon * sigma**6 * (rp**6*(14 * rc**3 - 63 * rc**2 * rp + 99 *
                                                                           rc * rp**2 - 55 * rp**3) + (-91 * rc**3 + 351 * rc**2 * rp - 459 * rc * rp**2 + 204 * rp**3) * sigma**6)

    return Apoli7, Bpoli7, Cpoli7, Dpoli7, Epoli7, Fpoli7, Gpoli7, Hpoli7


@jit
def poldegrees3(rp, rc, epsilon=0.345, sigma=2.644):
    minus = rp - rc
    plus = rp + rc
    ee = 4 * epsilon * ((sigma / rp)**12 - (sigma / rp)**6)
    ff = 4 * epsilon * (-12 * sigma**12 / rp**13 + 6 * sigma**6 / rp**7)
    dd = ff / minus**2 - 2 * ee * minus**3
    cc = ff / (2 * minus) - 3 / 2 * dd * plus
    aa = cc * rc**2 - 2 * dd * rc**3
    bb = -2 * cc * rc - 3 * dd * rc**3
    return aa, bb, cc, dd, ee, ff


@jit(nopython=True, cache=True)
def polforce(totdist, rp, rc):
    """Compute the forces using a 7th degree polnomial 
    as a junction for the lj potential
    """
    # Trying with a 3 deg pol
    # degrees = poldegrees3(rp, rc)
    # aa, bb, cc, dd, ee, ff = degrees
    # return - (bb / totdist + 2 * cc + 3 * dd * totdist)
    degrees = poldegrees(rp, rc)
    A, B, C, D, E, F, G, H = degrees
    return - (B / totdist + 2 * C + 3 * D * totdist + 4 * E * totdist**2
              + 5 * F * totdist**3 + 6 * G * totdist**4 + 7 * H * totdist**5)


@jit(nopython=True, cache=True)
def polpot(totdist, rp, rc):
    # degrees = poldegrees3(rp, rc)
    # aa, bb, cc, dd, ee, ff = degrees
    # return aa + bb * totdist + cc * totdist**2 * dd * totdist**3
    degrees = poldegrees(rp, rc)
    A, B, C, D, E, F, G, H = degrees
    return A + B * totdist + C * totdist**2 + D * totdist**3 + E * totdist**4 \
        + F * totdist**5 + G * totdist**6 + H * totdist**7


@jit(nopython=True, cache=True)
def calclj(positions, neighbors, rprime, rcutoff, sx, sy, sz, epsilon=0.345, sigma=2.644, type=7, pbc=False):
    """Compute the forces and the potential energy using the 
    Lennard-Jones potential. The type indicates which kind of 
    cutoff to use: if it's 7 then it uses a 7 order polynomial,
    and if it is -1 it uses a sharp cutoff
    """
    rc, rp = rcutoff, rprime
    natoms = positions.shape[0]
    # Initialize the forces as an empty array
    fx = np.zeros(natoms)
    fy = np.zeros(natoms)
    fz = np.zeros(natoms)
    epot = 0
    # For each neighbor of each single atom take the distance
    for i in range(natoms):
        ipos = positions[i]
        for j in range(neighbors.shape[1]):
            # neighbors is a  square matrix of natomsxnatoms. When there is a -1 it
            # means the neighbors are over and thus it's better to go to the next step
            if neighbors[i, j] == -1:
                continue
            jpos = positions[neighbors[i, j], :]
            # compute the distances between the i-th atom and the j-th atom
            totdist = calcdist(ipos, jpos)
            xdist = ipos[0] - jpos[0]
            ydist = ipos[1] - jpos[1]
            zdist = ipos[2] - jpos[2]
            # TODO: add implementation of pbc
            if pbc:
                pass
            if totdist <= rp:
                # Compute the forces using the Lennard Jones potential
                fx[i] += ljforce(totdist, xdist)
                fy[i] += ljforce(totdist, ydist)
                fz[i] += ljforce(totdist, zdist)
                epot += ljpot(totdist)
            elif totdist <= rc:
                # Compute the forces using the 7th degree polinomial
                poljunction = polforce(totdist, rp, rc)
                fx[i] += poljunction * xdist
                fy[i] += poljunction * ydist
                fz[i] += poljunction * zdist
                # Compute also the potential using a 7 degree poli
                epot = epot + polpot(totdist, rp, rc)

    # Remember that the potential energy is 1/2 Sum\ otherwise you
    # count the interaction i-j and the j-i two times.
    epot = epot / 2

    return fx, fy, fz, epot


@jit(nopython=True)
def calcekin(vx, vy, vz, mass):
    natoms = vx.shape[0]
    v2 = vx**2 + vy**2 + vz**2
    ekin = 1 / 2 * mass * np.sum(v2)
    tinstant = 2 / 3 * 11603 * ekin / natoms
    return np.array([ekin, tinstant])
