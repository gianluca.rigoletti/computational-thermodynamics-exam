# How to run the code

- Clone the repo and cd in the `code` folder
- Run `python main.py`

# How to analyse the results of the simulation

- Run a jupyter notebook and open the file `code/analysis.ipynb`
- Alternatively, cd into the cloned folder and run `python analysis.py`
